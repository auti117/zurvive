package com.thirdspare.zurvive;

import java.awt.*;

public class Money {

    //Characteristics
    private double money;
    private double x;
    private double y;
    private int size = 16;

    public Money (double money, double x, double y) {
        this.money = money;
        this.x = x;
        this.y = y;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    Rectangle getBounds () {
        return new Rectangle((int) x, (int) y, size, size);
    }

    public boolean collision (Player p) {
        return p.getBounds().intersects(getBounds());
    }

}
