package com.thirdspare.zurvive;

import org.newdawn.slick.Graphics;

public class Button {
    private int xCoordinate;
    private int yCoordinate;
    private int width;
    private int height;

    public Button(int x, int y, int w, int h) {
        this.xCoordinate = x;
        this.yCoordinate = y;
        this.width = w;
        this.height = h;
    }

    public int getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(int xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public int getyCoordinate() {

        return yCoordinate;
    }

    public void setyCoordinate(int yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isSelected(int x, int y) {
        return (x > xCoordinate && x < xCoordinate + width && y > yCoordinate && y < yCoordinate + height);
    }

    public void draw(Graphics g) {
        g.fillRect(xCoordinate, yCoordinate, width, height);
    }
}
