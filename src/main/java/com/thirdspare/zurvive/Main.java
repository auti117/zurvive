package com.thirdspare.zurvive;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.ScalableGame;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import java.awt.*;
import java.io.IOException;

public class Main extends StateBasedGame {

    //State Variables

    public static final String gameTitle = "Zurvive";

    public Main(String name) {
        super(name);
        this.addState(new Menu());//Adds the state to the main game container
        this.addState(new Play());//Adds the state to the main game container
        this.addState(new Options());//Adds the state to the main game container
        this.addState(new GameOver());
        this.addState(new Upgrades());//Adds the state to the main game container
    }


    @Override
    public void initStatesList(GameContainer gameContainer) throws SlickException {

        this.getState(GameVariable.MENU_STATE).init(gameContainer, this);//Gets the state of the main menu
        this.getState(GameVariable.PLAY_STATE).init(gameContainer, this);//Gets the state of the game
        this.getState(GameVariable.OPTIONS_STATE).init(gameContainer, this);//Gets the state of the option menu
        this.getState(GameVariable.GO_STATE).init(gameContainer, this);//Game over state

        this.enterState(GameVariable.MENU_STATE);//Sets the state of the main game container
    }

    public static void main(String[] args) throws IOException {

        LibHelper.loadLibs();//Loads the required libs


        //Makes the game container
        try {
            AppGameContainer container = new AppGameContainer(new ScalableGame(new Main(gameTitle), 600, 800, true));
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            if (screenSize.width > 4000) {
                container.setDisplayMode(1200, 1600, false);
            } else {
                container.setDisplayMode(600, 800, false);
            }
            container.setAlwaysRender(true);//Always renders in the background
            container.setShowFPS(true);//Disables the FPS
            container.start();//Starts the game
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

}
