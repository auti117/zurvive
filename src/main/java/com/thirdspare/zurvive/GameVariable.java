package com.thirdspare.zurvive;

import org.newdawn.slick.Input;

public class GameVariable {
    //Game states
    public static final int MENU_STATE = 0;
    public static final int PLAY_STATE = 1;
    public static final int OPTIONS_STATE = 2;
    public static final int GO_STATE = 3;
    public static final int UPGRADES_STATE = 4;

    //Key bindings
    public static int keyUp = Input.KEY_W;
    public static int keyDown = Input.KEY_S;
    public static int keyLeft = Input.KEY_A;
    public static int keyRight = Input.KEY_D;
    public static int keyShoot = Input.KEY_SPACE;
    public static int keyInteract = Input.KEY_E;
    public static int keyBbutton = Input.KEY_B;
    public static int mouseLeft = Input.MOUSE_LEFT_BUTTON;

    //Images
    public static String backgroundImage = "background.png";
    public static String playButtonImage = "playbutton.png";
    public static String settingsButtonImage = "settingsbutton.png";
    public static String titleImage = "zurvive.png";
    public static String playerDown = "playerfrontview.png";
    public static String playerUp = "playerbackview.png";
    public static String playerLeft = "playerleftview.png";
    public static String playerRight = "playerrightview.png";
    public static String bulletImage = "bullet.png";
    public static String zombie = "zombie.png";
    public static String moneyImage = "money.png";
    public static String imgUpgradeBuckshot = "buckshot.png";
    public static String imgUpgradeMG = "machinegun.png";
    public static String imgBackButton = "back.png";
    public static String imgUpgradeBackground = "upgradebackground.png";


    //Strings
    public static String gameOverMessage = "GAME OVER";

    //Changeable Variables for price of upgrades
    //TODO - Change prices for balance, temp low for testing
    public static double buckShotPrice = 10;
    public static double machineGunPrice = 10;

    //Changeable boolean variables
    public static boolean buckShotPurchased = false;
    public static boolean machineGunPurcahsed = false;

    //Changeable Variables
    public static String shotType = "Normal";

    //Changeable Speed vairables
    public static double zombieSpawnRate = 200;
    public static double bank;
    public static double projecttileDamage = 10;
    public static double shotSpeed = 7;
    public static double playerSpeed = 2;
    public static double shotVel = 10;
}
