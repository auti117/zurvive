package com.thirdspare.zurvive;

public class Barrier {
    private double HP;
    private boolean spiked;

    public Barrier() {
        this.HP = 1000;
        this.spiked = false;
    }

    public double getHP() {
        return HP;
    }

    public boolean isSpiked() {
        return spiked;
    }

    public void setHP(double hp) {
        this.HP = hp;
    }

    public void setSpiked(boolean s) {
        this.spiked = s;
    }
}
