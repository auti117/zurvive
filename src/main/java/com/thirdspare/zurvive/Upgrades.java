package com.thirdspare.zurvive;

import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class Upgrades extends BasicGameState {

    //Object References
    private Button backButton;
    private Button buckShotUpgrade;
    private Button machineGunUpgrade;

    //Images
    private Image upgradeImgBuckshot;
    private Image upgradeImgMG;
    private Image upgradeImgBack;
    private Image background;

    private boolean purchased;
    private boolean notEnoughMoney;
    private double time = 0;

    @Override
    public int getID() {
        return 4;
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {

        //Creating buttons to click on for upgrades
        backButton = new Button(50, 150, 100, 75);
        buckShotUpgrade = new Button(200, 150, 100, 75);
        machineGunUpgrade = new Button(350, 150, 100, 75);

        //Creating images
        upgradeImgBuckshot = new Image(GameVariable.imgUpgradeBuckshot);
        upgradeImgBack = new Image(GameVariable.imgBackButton);
        upgradeImgMG = new Image(GameVariable.imgUpgradeMG);
        background = new Image(GameVariable.imgUpgradeBackground);

        //If the player bought an item or does not have enough money for one
        purchased = false;
        notEnoughMoney = false;


    }

    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {

        graphics.clear();

        graphics.drawImage(background, 0, 0);
        graphics.drawImage(upgradeImgBack, 100, 150);

        graphics.drawImage(upgradeImgBuckshot, 250, 150);
        graphics.drawString("Price : $" + GameVariable.buckShotPrice, 240, 220);

        //TODO - Add Image for machine gun upgrade
        graphics.drawImage(upgradeImgMG, 400, 150);
//        graphics.drawString("Machine Gun", 350, 180);
        graphics.drawString("Price : $" + GameVariable.machineGunPrice, 390, 220);

        //Text based
        graphics.drawString("Money : $" + GameVariable.bank, 240, 125);

        if (purchased)
            graphics.drawString("Purchased!", 250, 400);

        if (notEnoughMoney){
            graphics.drawString("Not Enough Money!", 230, 400);
        }

    }

    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int delta) throws SlickException {

        Input input = gameContainer.getInput();

        if (notEnoughMoney || purchased)
            time += delta;

        if (backButton.isSelected(input.getMouseX(), input.getMouseY()) && input.isMouseButtonDown(0))
            stateBasedGame.enterState(1);

        //Buying Buck Shot
        if (buckShotUpgrade.isSelected(input.getMouseX(), input.getMouseY()) && input.isMouseButtonDown(0) && !GameVariable.buckShotPurchased) {
            if (GameVariable.bank >= GameVariable.buckShotPrice) {
                GameVariable.shotType = "Buck Shot";
                GameVariable.shotSpeed = 20;
                GameVariable.buckShotPurchased = true;
                GameVariable.projecttileDamage = 10;
                purchased = true;
                GameVariable.bank -= GameVariable.buckShotPrice;
            } else
                notEnoughMoney = true;
        }

        //Buying Machine Gun
        if (machineGunUpgrade.isSelected(input.getMouseX(), input.getMouseY()) && input.isMouseButtonDown(0) && !GameVariable.machineGunPurcahsed){
            if (GameVariable.bank >= GameVariable.machineGunPrice) {
                GameVariable.shotType = "Machine Gun";
                GameVariable.shotSpeed = 0.5;
                GameVariable.machineGunPurcahsed = true;
                GameVariable.projecttileDamage = 5;
                GameVariable.bank -= GameVariable.machineGunPrice;
                purchased = true;
            } else
                notEnoughMoney = true;
        }

        if (purchased && time > 3000) { //Removes purchased Notification
            purchased = false;
            time = 0;
        }
        if (notEnoughMoney && time > 3000) {//Removes not enough money notification
            notEnoughMoney = false;
            time = 0;
        }
    }
}
