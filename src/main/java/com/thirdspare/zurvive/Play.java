package com.thirdspare.zurvive;

import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

public class Play extends BasicGameState {

    //Input of all cases
    private  Input input;

    //Boolean variables
    private boolean nextRound; //True/False to halt zombies from spawning
    private boolean canBuy;

    //Creating reference object
    private Player player; //Creates player object
    private Barrier barrier; //Creates barrier object
    private Button shopArea; //Creates an area of where the buy-able area will be

    //Array Lists for objects
    private ArrayList<Zombie> zombies = new ArrayList<>(); //Array list of zombies
    private ArrayList<Projectile> shot = new ArrayList<>(); //Array list for bullets
    private ArrayList<Money> money = new ArrayList<>(); //Array list for money

    //Variables that deal with zombies what so ever
    private int amountZoms = 100;
    private int currentAmount = 0;
    private double timePassed = 0;
    private double timePassed2 = 0;
    private int roundNum = 0;

    //Zombie Modifiers
    private int zombieHP;
    private double zombieSpeed;
    private double zombieDamage;
    private int amountZomsKilled;

    //Vertical and Horizontal velocities to reach mouse location
    private double velX;
    private double velY;
    private double angle;

    //Images
    private Image playerUp;
    private Image playerDown;
    private Image playerLeft;
    private Image playerRight;
    private Image zombie;
    private Image bulletImage;
    private Image use;
    private Image backgroundImage;
    private Image moneyImage;

    //Format
    private DecimalFormat format = new DecimalFormat("0.00");

    @Override
    public int getID() {
        return 1;
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {

        //Player
        player = new Player(300, 400); //Initialize player position to the center of the screen
        //Barrier
        barrier = new Barrier();
        //Shop area
        shopArea = new Button(450, 680, 150, 120);

        //Inital Zombie Stats
        zombieHP = 10;
        zombieSpeed = 1;
        zombieDamage = 0.001;
        amountZomsKilled = 0;

        //Getting images
        playerDown = new Image(GameVariable.playerDown);
        playerUp = new Image(GameVariable.playerUp);
        playerLeft = new Image(GameVariable.playerLeft);
        playerRight = new Image(GameVariable.playerRight);
        bulletImage = new Image(GameVariable.bulletImage);
        backgroundImage = new Image(GameVariable.backgroundImage);
        use = playerUp;
        zombie = new Image(GameVariable.zombie);
        moneyImage = new Image(GameVariable.moneyImage);

        //Amount of zombies to generate per round
        roundNum = 1;
        nextRound = true;
    }

    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {

        graphics.clear();

        //Barrier Render
        graphics.drawImage(backgroundImage, 0, 0);
        graphics.drawString("Hp : " + format.format(barrier.getHP()), 0, 650);

        //Player Render
        graphics.drawImage(use, (float) player.getX(), (float) player.getY());

        if (canBuy)
            graphics.drawString("Press B to buy upgrades", 200, 400);

        //Reminding the player to hit enter to start next round - Temporary
        if (!nextRound)
            graphics.drawString("Press ENTER to start next round", 150, 414);
        //Zombies render
        for (Zombie zomby : zombies)
            graphics.drawImage(zombie, (float) zomby.getxCoordinate(), (float) zomby.getyCoordinate());

        //Generates Bullet Shot
        for (Projectile bullet : shot)
            graphics.drawImage(bulletImage, (float) bullet.getxCoordinate(), (float) bullet.getyCoordinate());

        //Money drop render
        for (Money moneyDrop : money)
            graphics.drawImage(moneyImage, (float) moneyDrop.getX(), (float) moneyDrop.getY());

        //Displays important information about the zombies and player
        graphics.drawString("Round : " + roundNum, 10, 24);
        graphics.drawString("Zombies : " + amountZoms, 10, 38);
        graphics.drawString("Zombie Speed : " + format.format(zombieSpeed), 10, 52);
        graphics.drawString("Zombie Health : " + zombieHP, 10, 66);
        graphics.drawString("Zombie Damage : " + format.format(zombieDamage), 10, 80);
        graphics.drawString("Amount Remaining : " + (amountZoms - amountZomsKilled), 10, 94);
        graphics.drawString("Money : " + format.format(GameVariable.bank), 450, 10);
    }

    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int delta) throws SlickException {

        input = gameContainer.getInput();

        if (input.isKeyPressed(GameVariable.keyBbutton) && canBuy) { //Goes into the upgrades State to get upgrades

            stateBasedGame.enterState(GameVariable.UPGRADES_STATE);

        }

        //Updating time passed
        timePassed += delta;
        timePassed2++;

        //Spawning new Zombies
        if (timePassed > GameVariable.zombieSpawnRate && currentAmount < amountZoms && nextRound) {
            zombies.add(new Zombie(zombieHP));
            timePassed = 0;
            currentAmount++;
        }

        //TODO - Temp start next round button
        if (input.isKeyPressed(Input.KEY_ENTER)) {
            nextRound = true;
            money.removeAll(money);
        }

        //Player Movement
        if (input.isKeyDown(GameVariable.keyUp)) {//Moves Up
            player.setY(player.getY() - GameVariable.playerSpeed);
            use = playerUp;
        }

        if (input.isKeyDown(GameVariable.keyDown)) {//Moves Down
            player.setY(player.getY() + GameVariable.playerSpeed);
            use = playerDown;
        }

        if (input.isKeyDown(GameVariable.keyRight)) {//Moves Right
            player.setX(player.getX() + GameVariable.playerSpeed);
            use = playerRight;
        }

        if (input.isKeyDown(GameVariable.keyLeft)) {//Moves Left
            player.setX(player.getX() - GameVariable.playerSpeed);
            use = playerLeft;
        }

        //Checks to see if player is within buying area
        canBuy = shopArea.isSelected((int) player.getX(), (int) player.getY());

        //Boundaries
        if (player.getX() < 0)
            player.setX(16);
        if (player.getX() > 600)
            player.setX(584);
        if (player.getY() < 0)
            player.setY(16);
        if (player.getY() > 800)
            player.setY(784);

        //Shooting movement
        if (input.isMouseButtonDown(GameVariable.mouseLeft)) {
            typeShot(); //Calls type shot method to decide what shot to use
            if (GameVariable.machineGunPurcahsed) //Reduces Player speed when shooting the machine gun
                GameVariable.playerSpeed = 0.5;
        } else
            GameVariable.playerSpeed = 2; //Returns to regular movement speed

        try { //Needed due to no shot initially
            for (Projectile aShot : shot) {
                aShot.setxCoordinate(aShot.getxCoordinate() + aShot.getVelX());
                aShot.setyCoordinate(aShot.getyCoordinate() + aShot.getVelY());
            }
        } catch (ArrayIndexOutOfBoundsException e) {
        }

        //Zombie movement
        for (Zombie zomby1 : zombies) {
            zomby1.setyCoordinate(zomby1.getyCoordinate() + zombieSpeed);
            if (zomby1.getyCoordinate() >= 670) {
                barrier.setHP(barrier.getHP() - zombieDamage);
                zomby1.setyCoordinate(670);
            }
        }

        //Checks the barriers health, and will go to the game over screen if it reaches 0
        if (barrier.getHP() <= 0)
            stateBasedGame.enterState(GameVariable.GO_STATE);

        //Collision of zombie to bullet
        Iterator<Projectile> projectileIterator = shot.iterator();
        while (projectileIterator.hasNext()) {
            Projectile p = projectileIterator.next();

            if (p.getyCoordinate() < 0 || p.getyCoordinate() > 800
                    || p.getxCoordinate() < 0 || p.getxCoordinate() > 600) {
                //If the bullet misses and goes past the screen
                projectileIterator.remove();
            }

            Iterator<Zombie> zombieIterator = zombies.iterator();
            while (zombieIterator.hasNext()) {
                Zombie z = zombieIterator.next();

                try {
                    if (p.collision(z)) {
                        projectileIterator.remove();
                        int hp = z.getHP();
                        hp -= GameVariable.projecttileDamage;
                        z.setHP(hp);

                        if (z.getHP() <= 0) {
                            if (z.isMoney())//If the zombie can drop money
                                money.add(new Money(10, z.getxCoordinate(), z.getyCoordinate()));

                            zombieIterator.remove(); //Kills zombie
                            amountZomsKilled++; //Adds kill count

                            if (amountZomsKilled == amountZoms) //Checks to see if the player cleared the round
                                endOfRound();
                        }
                    }
                } catch (IllegalStateException e) {
                }
            }
        }

        //Sees if the player walks over the money
        Iterator<Money> moneyIterator = money.iterator();
        while (moneyIterator.hasNext()) {
            Money m = moneyIterator.next();

            try {
                if (m.collision(player)) {
                    GameVariable.bank += m.getMoney();
                    moneyIterator.remove(); //Removes money object
                }
            } catch (IllegalStateException e) {
            }
        }
    }

    private void endOfRound() { //Method to read for when a round has been completed

        System.err.println("End of round");
        roundNum++; //Increase the round number

        if (roundNum % 10 == 0) { //Every 10 rounds increase speed health and damage
            zombieSpeed += 0.05;
            zombieDamage += 0.01;
            zombieHP += 10;
            GameVariable.zombieSpawnRate *= 0.75;
        }

        //Increase numbers of zombies every round by 50
        amountZoms += 50;
        amountZomsKilled = 0;
        currentAmount = 0; //Reset current amount of zombies
        nextRound = false; //Halts the spawn of zombies

    }

    private void typeShot() { //Method to see what kind of bullet is being shot

        //Quick Maths
        angle = Math.atan2(input.getMouseY() - player.getY(), input.getMouseX() - player.getX());
        velX = GameVariable.shotVel * Math.cos(angle);
        velY = GameVariable.shotVel * Math.sin(angle);

        if (timePassed2 % GameVariable.shotSpeed == 0) { //Makes sure that the bullets spawn at an even interval
            switch (GameVariable.shotType) {

                case "Normal" :
                    shot.add(new Projectile(player.getX(), player.getY(), 4, false, velX, velY));
                    break;

                case "Buck Shot" :
                    shot.add(new Projectile(player.getX(), player.getY(),4,false, velX, velY));
                    shot.add(new Projectile(player.getX(), player.getY(),4,false, velX + 1, velY));
                    shot.add(new Projectile(player.getX(), player.getY(),4,false, velX - 1, velY));
                    break;

                case "Machine Gun" :
                    shot.add(new Projectile(player.getX(), player.getY(), 1, false, velX + (Math.random() * 2), velY + (Math.random() * 2)));
                    break;

            }
            timePassed2 = 0;
        }
    }
}