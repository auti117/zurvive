package com.thirdspare.zurvive;

import java.awt.*;

public class Player {

    //Current Player's X and Y position
    private double x;
    private double y;
    public static int size = 32;

    public Player(double x, double y) {

        this.x = x;
        this.y = y;

    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Rectangle getBounds() {
        return new Rectangle((int) x, (int) y, size, size);
    }
}
