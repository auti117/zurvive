package com.thirdspare.zurvive;

import java.awt.*;

public class Zombie {
    private int HP;
    private double xCoordinate;
    private double yCoordinate;
    private static int size;
    private boolean money;

    Zombie(int HP) {
        this.HP = HP;
        this.xCoordinate = Math.random() * 584;
        this.yCoordinate = 0;
        this.size = 32;
        this.money = ((int) (Math.random() * 10) == 5);
    }

    int getHP() {
        return this.HP;
    }

    double getxCoordinate() {
        return this.xCoordinate;
    }

    double getyCoordinate() {
        return this.yCoordinate;
    }

    void setHP(int hp) {
        this.HP = hp;
    }

    public void setxCoordinate(double x) {
        this.xCoordinate = x;
    }

    void setyCoordinate(double y) {
        this.yCoordinate = y;
    }

    public int dropValue() {
        return (int) (Math.random() * 14 + 1);
    }

    Rectangle getBounds() {
        return new Rectangle((int) xCoordinate, (int) yCoordinate, size, size);
    }

    public boolean collision(Projectile bullet) {
        return bullet.getBounds().intersects(getBounds());
    }

    public boolean isMoney() {
        return money;
    }

    //TODO Add player and Zombie collision
}