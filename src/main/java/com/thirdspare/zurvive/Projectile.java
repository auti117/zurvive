package com.thirdspare.zurvive;

import java.awt.*;

public class Projectile {
    private double xCoordinate;
    private double yCoordinate;
    private int size;
    private boolean explosive;

    private double velX;
    private double velY;

    public Projectile(double x, double y, int s, boolean e, double velX, double velY) {
        this.xCoordinate = x;
        this.yCoordinate = y;
        this.size = s;
        this.explosive = e;
        this.velX = velX;
        this.velY = velY;
    }

    public double getxCoordinate() {
        return xCoordinate;
    }

    public boolean isExplosive() {
        return explosive;
    }

    public int getSize() {

        return size;
    }

    public double getyCoordinate() {

        return yCoordinate;
    }

    public void setxCoordinate(double x) {
        this.xCoordinate = x;
    }

    public void setyCoordinate(double y) {
        this.yCoordinate = y;
    }

    public void setSize(int s) {
        this.size = s;
    }

    public void setExplosive(boolean e) {
        this.explosive = e;
    }

    public Rectangle getBounds() {
        return new Rectangle((int) xCoordinate, (int) yCoordinate, size, size);
    }

    public boolean collision(Zombie zombie) {
        return zombie.getBounds().intersects(getBounds());//Collision with Zombie
    }

    public boolean collision(Player p){//Collision with player.
        return p.getBounds().intersects(getBounds());
    }

    public double getVelX() {
        return velX;
    }

    public void setVelX(double velX) {
        this.velX = velX;
    }

    public double getVelY() {
        return velY;
    }

    public void setVelY(double velY) {
        this.velY = velY;
    }
}