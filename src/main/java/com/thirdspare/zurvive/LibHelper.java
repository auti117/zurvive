package com.thirdspare.zurvive;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public abstract class LibHelper {


    private static final String[] WINDOWS_NATIVES = {
            "lwjgl.dll",
            "lwjgl64.dll",
            "OpenAL32.dll",
            "OpenAL64.dll",
            "jinput-dx8.dll",
            "jinput-dx8_64.dll",
            "jinput-raw.dll",
            "jinput-raw_64.dll",
            "jinput-wintab.dll"
    };

    public static void loadLibs() throws IOException {
        File targetFolder = new File("lib/natives/");
        System.setProperty("org.lwjgl.librarypath", targetFolder.getAbsolutePath());
        System.setProperty("net.java.games.input.librarypath", targetFolder.getAbsolutePath());
        targetFolder.mkdirs();

        for (String nat : WINDOWS_NATIVES) {

            File targetFile = new File(targetFolder, nat);
            if (!targetFile.exists()) {
                InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(nat);

                Files.copy(is, targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

            }
        }

    }
}