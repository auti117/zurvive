package com.thirdspare.zurvive;

import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class Menu extends BasicGameState {
    private static Button playButton = new Button(150, 300, 300, 74);
    private static Button optionsButton = new Button(150, 400, 300, 84);
    private static int playGameX = 150;
    private static int playGameY = 150;
    private Image playButtonImage;
    private Image optionsButtonImage;
    private Image backgroundImage;
    private Image titleImage;

    @Override
    public int getID() {
        return 0;
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        playButtonImage = new Image(GameVariable.playButtonImage);
        optionsButtonImage = new Image(GameVariable.settingsButtonImage);
        backgroundImage = new Image(GameVariable.backgroundImage);
        titleImage = new Image(GameVariable.titleImage);
    }

    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {

        graphics.clear();
        graphics.drawImage(backgroundImage, 0, 0);
        graphics.drawImage(titleImage, 125, 100);
        graphics.drawImage(playButtonImage, playButton.getxCoordinate(), playButton.getyCoordinate());
        graphics.drawImage(optionsButtonImage, optionsButton.getxCoordinate(), optionsButton.getyCoordinate());

    }

    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {
        Input input = gameContainer.getInput();

        input.getMouseX();
        input.getMouseY();

        gameContainer.setVSync(true); //So that game is balanced throughout all platforms
        gameContainer.setShowFPS(false);

        if (playButton.isSelected(input.getMouseX(), input.getMouseY()) && input.isMouseButtonDown(0)) {
            stateBasedGame.enterState(GameVariable.PLAY_STATE);
        }
        if (optionsButton.isSelected(input.getMouseX(), input.getMouseY()) && input.isMouseButtonDown(0)) {
            stateBasedGame.enterState(GameVariable.OPTIONS_STATE);
        }
    }
}

