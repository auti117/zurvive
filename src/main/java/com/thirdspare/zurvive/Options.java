package com.thirdspare.zurvive;

import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class Options extends BasicGameState {
    private Button setUp = new Button(100, 90, 400, 80);
    private Button setDown = new Button(100, 190, 400, 80);
    private Button setLeft = new Button(100, 290, 400, 80);
    private Button setRight = new Button(100, 390, 400, 80);
    private Button setShoot = new Button(100, 490, 400, 80);
    private Button setInteract = new Button(100, 590, 400, 80);

    @Override
    public int getID() {
        return 2;
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {

    }

    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {
        graphics.setBackground(Color.gray);
        graphics.clear();
        graphics.setColor(Color.black);

        //For expansion in future
        /*setUp.draw(graphics);
        setDown.draw(graphics);
        setLeft.draw(graphics);
        setRight.draw(graphics);
        setShoot.draw(graphics);
        setInteract.draw(graphics);
        graphics.setColor(Color.white);
        graphics.drawString("Set Key: Up", setUp.getxCoordinate(), setUp.getHeight() / 3 + setUp.getyCoordinate());
        graphics.drawString("Set Key: Down", setDown.getxCoordinate(), setDown.getHeight() / 3 + setDown.getyCoordinate());
        graphics.drawString("Set Key: Left", setLeft.getxCoordinate(), setLeft.getHeight() / 3 + setLeft.getyCoordinate());
        graphics.drawString("Set Key: Right", setRight.getxCoordinate(), setRight.getHeight() / 3 + setRight.getyCoordinate());
        graphics.drawString("Set Key: Shoot", setShoot.getxCoordinate(), setShoot.getHeight() / 3 + setShoot.getyCoordinate());
        graphics.drawString("Set Key: Interact", setInteract.getxCoordinate(), setInteract.getHeight() / 3 + setInteract.getyCoordinate());*/
    }

    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {
        Input input = gameContainer.getInput();
    }
}
